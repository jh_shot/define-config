import { BabelConfig, CommitlintConfig, CzCustomizableConfig, EslintConfig, PostcssConfig, PrettierConfig, StylelintConfig } from './types';

export declare function defineCommitlintConfig(config: CommitlintConfig): CommitlintConfig;
export declare function defineCzCustomizableConfig(config: CzCustomizableConfig): CzCustomizableConfig;
export declare function definePrettierConfig(config: PrettierConfig): PrettierConfig;
export declare function defineEslintConfig(config: EslintConfig): EslintConfig;
export declare function definePostcssConfig(config: PostcssConfig): PostcssConfig;
export declare function defineStylelintConfig(config: StylelintConfig): StylelintConfig;
export declare function defineBabelConfig(config: BabelConfig): BabelConfig;

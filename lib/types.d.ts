import { Config as _StylelintConfig } from 'stylelint';
import { Config as _PrettierConfig } from 'prettier';
import { AcceptedPlugin, ProcessOptions } from 'postcss';
import { Linter as _EslintConfig } from 'eslint';
import { Options } from 'cz-customizable';
import { UserConfig as _CommitlintConfig } from '@commitlint/types';
import { TransformOptions as _BabelConfig } from '@babel/core';

export type CommitlintConfig = _CommitlintConfig;
export type CzCustomizableConfig = Options;
export type PrettierConfig = _PrettierConfig;
export type EslintConfig = _EslintConfig.Config;
export type PostcssConfig = ProcessOptions & {
    plugins: AcceptedPlugin[];
};
export type StylelintConfig = _StylelintConfig;
export type BabelConfig = _BabelConfig;

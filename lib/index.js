function e(n) {
  return n;
}
function i(n) {
  return n;
}
function t(n) {
  return n;
}
function f(n) {
  return n;
}
function o(n) {
  return n;
}
function r(n) {
  return n;
}
function u(n) {
  return n;
}
export {
  u as defineBabelConfig,
  e as defineCommitlintConfig,
  i as defineCzCustomizableConfig,
  f as defineEslintConfig,
  o as definePostcssConfig,
  t as definePrettierConfig,
  r as defineStylelintConfig
};

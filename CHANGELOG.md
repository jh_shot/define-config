# @ttou/define-config

## 3.3.0

### Minor Changes

- 移除 lint-staged 和 simple-git-hooks

## 3.2.0

### Minor Changes

- 优化

## 3.1.0

### Minor Changes

- 移除 Jest
- 移除 Rollup
- 新增 Simple Git Hooks

## 3.0.0

### Major Changes

- lint-staged 升级到 13, prettier 升级到 3

## 2.1.3

### Patch Changes

- 移除多余依赖

## 2.1.2

### Patch Changes

- 更新依赖和打包配置

## 2.1.1

### Patch Changes

- 修复 cz-customizable 配置函数名

## 2.1.0

### Minor Changes

- 增加 cz-customozable 配置函数

## 2.0.6

### Patch Changes

- 使用 changeset 管理更改记录

import type {
  BabelConfig,
  CommitlintConfig,
  CzCustomizableConfig,
  EslintConfig,
  PostcssConfig,
  PrettierConfig,
  StylelintConfig
} from './types'

export function defineCommitlintConfig(
  config: CommitlintConfig
): CommitlintConfig {
  return config
}

export function defineCzCustomizableConfig(
  config: CzCustomizableConfig
): CzCustomizableConfig {
  return config
}

export function definePrettierConfig(config: PrettierConfig): PrettierConfig {
  return config
}

export function defineEslintConfig(config: EslintConfig): EslintConfig {
  return config
}

export function definePostcssConfig(config: PostcssConfig): PostcssConfig {
  return config
}

export function defineStylelintConfig(
  config: StylelintConfig
): StylelintConfig {
  return config
}

export function defineBabelConfig(config: BabelConfig): BabelConfig {
  return config
}

import type { TransformOptions as _BabelConfig } from '@babel/core'
import type { UserConfig as _CommitlintConfig } from '@commitlint/types'
import type { Options } from 'cz-customizable'
import type { Linter as _EslintConfig } from 'eslint'
import type { AcceptedPlugin, ProcessOptions } from 'postcss'
import type { Config as _PrettierConfig } from 'prettier'
import type { Config as _StylelintConfig } from 'stylelint'

export type CommitlintConfig = _CommitlintConfig

export type CzCustomizableConfig = Options

export type PrettierConfig = _PrettierConfig

export type EslintConfig = _EslintConfig.Config

export type PostcssConfig = ProcessOptions & {
  plugins: AcceptedPlugin[]
}

export type StylelintConfig = _StylelintConfig

export type BabelConfig = _BabelConfig

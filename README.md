# Define Config

配置文件定义辅助工具

| 包名             | 版本      | 配置格式 |
| ---------------- | --------- | -------- |
| eslint           | >= 8.0.0  | cjs      |
| prettier         | >= 3.0.0  | cjs、esm |
| postcss          | >= 8.0.0  | cjs      |
| commitlint       | >= 17.0.0 | cjs      |
| cz-customizable  | >= 7.0.0  | cjs      |
| babel            | >= 7.0.0  | cjs      |
| stylelint        | >= 14.0.0 | cjs      |
| lint-staged      | >= 13.0.0 | cjs、esm |
| simple-git-hooks | >= 13.0.0 | cjs      |

## 说明

- 从 3.1.0 开始移除了 Jest，推荐使用 Vitest
- 从 3.1.0 开始移除了 Rollup，推荐使用 Vite 或 tsup 等新型打包工具
- 像 `lint-staged` 和 `simple-git-hooks` 这种简单格式的配置推荐直接写在 `package.json` 里

## 示例

### CJS

```js
const { defineEslintConfig } = require('@ttou/define-config')

module.exports = defineEslintConfig({
  // 具体配置
})
```

### ESM

```js
import { definePrettierConfig } from '@ttou/define-config'

export default definePrettierConfig({
  // 具体配置
})
```
